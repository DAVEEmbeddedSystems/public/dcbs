
Clone this repo and build your container with

```
docker build -t dave/xelk:latest .
```

The docker image will be tagged as `dave/xelk:latest`

Once the docker image is ready, user can run commands inside it with `dcbs` utility
script.

To run commands inside the container, first of all you need to export some variable:

```bash
export DCBS_IMAGE=dave/xelk
export DCBS_VOL=$(pwd)
export PATH=PATH_TO_DCBS_SCRIPT:$PATH
```

Now you can type commands with `dcbs` prefix, e.g.:

```bash
dcbs make xxx_defconfig
```

or

```bash
dcbs make zImage
```
